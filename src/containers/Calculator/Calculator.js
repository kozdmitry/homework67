import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import "./Calculator.css";

const Calculator = () => {
    const dispatch = useDispatch();
    const counter = useSelector(state => state.counter);

    const input = useSelector(state => state);

    const increaseCounter = () => dispatch({type: 'INCREMENT'});
    const decreaseCounter = () => dispatch({type: 'DECREMENT'});
    const minusCounter = () => dispatch({type: 'SUBTRACT', value: 5});
    const plusCounter = () => dispatch({type: 'ADD', value: 5});


    const firstCalc = () => dispatch ({type: 'FIRST', value: 1});
    const secondCalc = () => dispatch ({type: 'SECOND', value: 2});
    const thirdCalc = () => dispatch ({type: 'THIRD', value: 3});
    const fourthCalc = () => dispatch ({type: 'FOURTH', value: 4});
    const fifthCalc = () => dispatch ({type: 'FIFTH', value: 5});
    const sixthCalc = () => dispatch ({type: 'SIXTH', value: 6});
    const seventhCalc = () => dispatch ({type: 'SEVENTH', value: 7});
    const eighthCalc = () => dispatch ({type: 'EIGHTH', value: 8});
    const ninthCalc = () => dispatch ({type: 'NINTH', value: 9});
    const delCalc = () => dispatch ({type: 'DEL'});
    const clearCalc = () => dispatch ({type: 'CLEAR'});



    return (
        <div className="Calculator">

            <h1>{counter}</h1>
            <button onClick={increaseCounter}>Increase</button>
            <button onClick={decreaseCounter}>Decrease</button>
            <button onClick={plusCounter}>Increase by 5</button>
            <button onClick={minusCounter}>Increase by 5</button>

        <div className="calc">
            <input type="text"/>
            <div className="inputCode">
            <span>{input.inputValue}</span>
            </div>
            <div className="firstString">
                <button onClick={firstCalc} className="btn"><span>1</span></button>
                <button onClick={secondCalc} className="btn"><span>2</span></button>
                <button onClick={thirdCalc} className="btn"><span>3</span></button>
                <button onClick={thirdCalc} className="btn"><span>/</span></button>
            </div>
            <div className="firstString">
                <button className="btn"><span>4</span></button>
                <button className="btn"><span>5</span></button>
                <button className="btn"><span>6</span></button>
                <button className="btn"><span>*</span></button>
            </div>
            <div className="firstString">
                <button className="btn"><span>7</span></button>
                <button className="btn"><span>8</span></button>
                <button className="btn"><span>9</span></button>
                <button className="btn"><span>+</span></button>

            </div>
            <div className="firstString">
                <button className="btn"><span>0</span></button>
                <button className="btn"><span>00</span></button>
                <button className="btn"><span>-</span></button>
                <button className="btn"><span>=</span></button>
            </div>
        </div>


        </div>
    );
};

export default Calculator;