const initialState = {
    inputValue: "",
    trueCombination: '1234',
    access: false,
    disabled: false
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case 'ADD_NUM':
            return {...state, inputValue: state.inputValue += action.num}
        case 'CHECK':
            if(state.trueCombination === state.inputValue) {
                return {...state, access: true}
            }
            return state
        case 'DISABLE':
            if(state.inputValue.length === 4) {
                return {...state, disabled: true}
            }
            return state
        case 'DEL':
            return {...state, inputValue: state.inputValue.slice(0,-1)}
        default:
            return state;
    }
};

export default reducer;