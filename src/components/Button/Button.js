import React from 'react';
import "./Button.css";
import {useDispatch, useSelector} from "react-redux";

const Button = () => {
    const keyPad = ["1", "2", "3", "4", "5", "6", "7", "8", "9","0"];

    const state = useSelector(state => state);
    const dispatch = useDispatch();

    const enterNumber = (e) => {
        dispatch({type: 'ADD_NUM', num: e.target.value})
        dispatch({type: "DISABLE"})
    }

    const checkNum = () => {
        dispatch({type: 'CHECK'})
    }

    const delNum = () => {
        dispatch({type: 'DEL'})
    }

    return (
        <div className="code">
            <div className={state.access ? 'green' : 'red'}>
                {state.inputValue}
            </div>
            <div className="">
                {keyPad.map((item, index) => (
                    <button
                        key={index}
                        disabled={state.disabled}
                        className="btn"
                        value={item}
                        onClick={(e)=> enterNumber(e)}>{item}</button>
                ))}
                <button className="btn" onClick={checkNum}>E</button>
                <button className="btn" onClick={delNum}>del</button>
            </div>

        </div>
    );
};

export default Button;